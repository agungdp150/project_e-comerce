export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_FAIL = "REGISTER_FAIL";

export const GET_USER = "GET_USER";
export const GET_USER_FAIL = "GET_USER_FAIL";

export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAIL = "LOGIN_FAIL"

export const GET_PRODUCT = "GET_PRODUCT";
export const GET_PRODUCT_DETAIL = "GET_PRODUCT_DETAIL";