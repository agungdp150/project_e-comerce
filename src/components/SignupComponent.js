import React, { Component } from "react";
import {Link} from "react-router-dom";
import { connect } from "react-redux";
import {register} from "../store/actions/registerAction"
import "../assets/scss/SignupComponent.scss";

class SignupComponent extends Component {
  constructor(props) {
    super (props);

    this.state = {
      name : "",
      email : "",
      password : "",
      gender : "",
      role : ""
    }
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name] : e.target.value 
    })
  }

  handleGender = (e) => {
    // console.log (e.target.value)
    this.setState ({
      [e.target.name] : e.target.value
    })
  }

  handleRole = (e) => {
    this.setState({
      [e.target.name] : e.target.value 
    })
  }

  handleInput = (e) => {
    e.preventDefault();
    console.log ("oke")

    const regisInput = JSON.stringify({
      name : this.state.name,
      email : this.state.email,
      password : this.state.password,
      gender : this.state.gender,
      role : this.state.role
    })
    // console.log (regisInput);
    this.props.register(regisInput);
    this.props.history.push("/login");
  }



  render() {

    const {name, email, password} = this.state;
    
    return (
      <div className="signup-page">
        <div className="form">
          <h1>Sign Up</h1>
          <form className="signup-form" onSubmit={this.handleInput}>
            <input 
              type="text" 
              placeholder="Name"
              name = "name"
              required
              value = {name}
              onChange = {this.handleChange}
              />
            <input 
              type="email" 
              placeholder="Email"
              name = "email"
              required
              value = {email} 
              onChange = {this.handleChange}
              />
            <input 
              type="password" 
              placeholder="Password"
              name = "password"
              required
              value = {password} 
              onChange = {this.handleChange}
              />

            <h4>Gender</h4>

            <div className="cntr s-gender">
            <label for="opt1" className="radio">
              <input 
              type="radio" 
              name="gender"
              value= "Male"
              onChange={this.handleGender}
              className="hidden"
              id="opt1" 
              />
              <span className="label"></span>Male
            </label>
            <label for="opt2" className="radio">
              <input 
              type="radio" 
              name="gender"
              value="Female"
              onChange={this.handleGender}
              className="hidden" 
              id="opt2" 
              />
              <span className="label"></span>Female
            </label>
            <label for="opt3" className="radio">
              <input 
              type="radio" 
              name="gender"
              value = "other"
              onChange={this.handleGender}
              className="hidden" 
              id="opt3"
              />
              <span className="label"></span>Other
            </label>
          </div>

          <h4>Merchant</h4>

          <div className="cntr s-role">
            <label for="op1" className="radio">
              <input 
              type="radio" 
              name="role"
              value = "Customer"
              onChange = {this.handleRole} 
              id="op1" 
              className="hidden"
              />
              <span className="label"></span>Customer
            </label>
            <label for="op2" className="radio">
              <input 
              type="radio" 
              name="role"
              value = "Merchant"
              onChange = {this.handleRole} 
              id="op2" 
              className="hidden"/>
              <span className="label"></span>Merchant
            </label>
            
          </div>

            <button
              type = "submit"
            >Sign up
            </button>

            <p className="message">
              Already member? <Link to="/login">Sign in</Link>
            </p>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated : state.register1.isAuthenticated
  }
}

export default connect(
  mapStateToProps,
  {register}
) (SignupComponent);    // this.props.register(regisInput);
// this.props.history.push("/");
