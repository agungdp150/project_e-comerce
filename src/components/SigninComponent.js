import React, { Component } from "react";
import {Link, withRouter} from "react-router-dom";
import { connect } from "react-redux";
import { loginUser } from "../store/actions/loginAction.js"
import "../assets/scss/SigninComponent.scss";

class SigninComponent extends Component {
  constructor (props) {
    super (props);

    this.state ={
      email : "",
      password : ""
    }
  }


  handleChange = (e) => {
    this.setState ({
      [e.target.name] : e.target.value 
    });
  };

  handleInput = async (e) => {
    e.preventDefault ()
    // console.log ("test")
    const dataLogin = JSON.stringify({
      email: this.state.email,
      password : this.state.password,
    })
    // console.log (dataLogin);
    await  this.props.loginUser(dataLogin);
    if (this.props.login1) {
      this.props.history.push("/");
    } else (
      alert("Something went wrong!")
    )

  }

  
  render() {

    const {email, password} = this.state

    return (
      <div className="login-page">
        <div className="form">
          <h1>Sign In</h1>
          <form className="login-form" onSubmit={this.handleInput}>
            <input 
              type="email" 
              placeholder="email"
              name="email"
              value={email}
              onChange = {this.handleChange}
              />
            <input 
              type="password" 
              placeholder="password"
              name = "password" 
              value ={password} 
              onChange = {this.handleChange}
              />
            <div className="check">
              <input 
              type="checkbox" 
              /> Remember me
            </div>

            <button 
              type = "submit"
              >Sign In
            </button>
            <p className="message">
              <Link to="/">
                Forgot password
              </Link>
              <Link to="/">
                Have an account
              </Link>
            </p>
          </form>
        </div>
      </div>
    );
  }
}

  const mapStateToProps = state => {
    return {
      isAuthenticated : state.login1.isAuthenticated,
      login1 : state.login1.token
    }
  }

export default connect(
  mapStateToProps,
  { loginUser }
) (withRouter(SigninComponent));
