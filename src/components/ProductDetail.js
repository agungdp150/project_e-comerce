import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {connect} from "react-redux";
import { getDetail } from "../store/actions/getdetailAction.js";
import "../assets/scss/ProductDetails.scss";
import loading from "./../assets/designs/805.svg";
import { dispatch } from "rxjs/internal/observable/pairs";

class ProductDetail extends Component {
  componentDidMount() {
    const id = this.props.match.params.id;
    // console.log (id);
     this.props.getDetail(id);
  }
  render() {

    const {price, name, description, qtyStock, productImages} = this.props.details
    // console.log(this.props.details)
    return (
      <div className="product">
      <div className="split left">
        <div className="image">
        <img src={productImages !== undefined ? productImages[0].url : loading} alt={name} />
          <div className="flex-image">
            <div>
              <img src="https://vignette.wikia.nocookie.net/spongebob/images/2/2a/SpongeBob_SquarePants%28copy%290.png/revision/latest?cb=20160507142128" alt="images" />
            </div>
            <div>
              <img src="https://images-na.ssl-images-amazon.com/images/I/41isOEwa4PL._SY450_.jpg" alt="images" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/teepublic/image/private/s--6swAsCGs--/t_Preview/b_rgb:ffffff,c_limit,f_jpg,h_630,q_90,w_630/v1490217638/production/designs/1346781_1.jpg" alt="images" />
            </div>
          </div>
        </div>
      </div>
      <div className="split right">
        <div className="text">
          <p>Home</p>
          <p className="dot">.</p>
          <p>All Categories</p>
          <p className="dot">.</p>
          <p>Mens's Clothing & Accessories</p>
        </div>
        <div className="stars">
          <span className="fa fa-star checked"></span>
          <span className="fa fa-star checked"></span>
          <span className="fa fa-star checked"></span>
          <span className="fa fa-star"></span>
          <span className="fa fa-star"></span>
        </div>
        <div className="title">
          <h2>{description}</h2>
        </div>
        <div className="price">
          <h3>Rp{price}</h3>
        </div>
        <div className="color">
          <p>Color</p>
          <div className="colors-name">
            <button className="donker"></button>
            <button className="blue"></button>
            <button className="red"></button>
            <button className="orange"></button>
            <button className="yellow"></button>
            <button className="green"></button>
            <button className="purple"></button>
          </div>
          <div className="size">
            <p>Size</p>
            <div className="size-box">
              <button className="xs">XS</button>
              <button className="s">S</button>
              <button className="m">M</button>
              <button className="l">L</button>
              <button className="xl">XL</button>
              <button className="xxl">XXL</button>
            </div>
            <div className="quantity">
              {qtyStock}
              <input type="number" name="quantity" min="1" max="10" />
            </div>
            <div className="submit">
              <button className="submit-1">Add to cart</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    details : state.getdetail1.details
  }
}

const mapDispatchToProps = state => ({
  getDetail: (id) => dispatch(getDetail(id))
})

console.log (mapDispatchToProps)

export default connect(
  mapStateToProps,
  {getDetail}
) (withRouter(ProductDetail));


