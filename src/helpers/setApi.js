// User
export const getUser = `https://ga-ecommerce.herokuapp.com/api/user`
export const addCustomer = `https://ga-ecommerce.herokuapp.com/api/user`
export const loginUser = `https://ga-ecommerce.herokuapp.com/api/user/login`

// Merchant
export const addMerchant = `https://ga-ecommerce.herokuapp.com/api/user`

// Product
export const getProduct = `https://ga-ecommerce.herokuapp.com/api/product`
export const addProduct = `https://ga-ecommerce.herokuapp.com/api/product`
export const getProductDetail = `https://ga-ecommerce.herokuapp.com/api/product/5d817a4ef2115123c1c5a468`
export const updateProduct = `https://ga-ecommerce.herokuapp.com/api/product/5d817669dca52d22c7afb9f0`
export const deleteProduct = `https://ga-ecommerce.herokuapp.com/api/product/5d817a4ef2115123c1c5a468`

// Cart
export const getCart = `https://ga-ecommerce.herokuapp.com/api/cart`
export const addCart = `https://ga-ecommerce.herokuapp.com/api/cart`
export const deleteCart = `https://ga-ecommerce.herokuapp.com/api/cart/5d817a4ef2115123c1c5a468`
