import axios from "axios"
import setToken from "../../helpers/setToken.js";
import * as type from "../../types";

export const getUser = () => async dispatch => {
  if (localStorage.token) {
    setToken(localStorage.token);
  }

  try {
    const response = await axios.GET (`https://ga-ecommerce.herokuapp.com/api/user`, {
      headers : {
        "Content-Type" : "application/json"
        }
      }
    );
    console.log (response.data);
    dispatch({
      type : type.GET_USER,
      payload : response.data
    })
  } catch (error) {
    console.log (error.response);
    dispatch ({
      type : type.GET_USER_FAIL
    })
  }
};


export const loginUser = SigninComponent => async dispatch =>{
  try {
    const setting = {
      headers :{
        "Content-Type" : "application/json"
      }
    };
    const response = await axios.post (
      `https://ga-ecommerce.herokuapp.com/api/user/login`,
      SigninComponent,
      setting
    );
    dispatch ({
      type : type.LOGIN_SUCCESS,
      payload : response.data
    });
  } catch (err) {
    console.log (err.response.data);
    dispatch ({
      type : type.LOGIN_FAIL
    });
  }
};

