import {GET_PRODUCT} from "../../types"
import axios from "axios";

export const getProduct = () => async dispatch => {
  try {
    const response = await axios.get(`https://ga-ecommerce.herokuapp.com/api/product`);
    // console.log (response.data.data);
    dispatch({
      type : GET_PRODUCT,
      payload : response.data.data
    });
  } catch (error) {
    console.log (error.response.data);
  }
}