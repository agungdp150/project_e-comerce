import axios from "axios";
import {GET_PRODUCT_DETAIL} from "../../types";

export const getDetail = id => async dispatch => {
  try {
    const response = await axios.get(`https://ga-ecommerce.herokuapp.com/api/product/${id}`);
    // console.log ("ini loh datanya", response.data);
    dispatch({
      type : GET_PRODUCT_DETAIL,
      payload : response.data
    });
    }
    catch (error) {
      console.log (error.response.data);
  }
}