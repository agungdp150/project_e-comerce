import axios from "axios";
import {REGISTER_SUCCESS, REGISTER_FAIL} from "../../types";


export const register = SignupComponent => async dispatch => {
  // console.log(SignupComponent)
  try {

    const response = await axios.post (
      "https://ga-ecommerce.herokuapp.com/api/user", SignupComponent, {
        headers: {
          "Content-Type" : "application/json"
        }
      }
    );
    console.log (response.data);
    dispatch ({
      type : REGISTER_SUCCESS,
      payload : response.data
    });
  } catch (error) {
    console.log (error.response.data);
    dispatch ({
      type : REGISTER_FAIL
    })
  }
}




// await axios({
//   method : "POST",
//   url : `https://ga-ecommerce.herokuapp.com/api/user`,
//   headers : {
//     "Content-Type" : "application/json"
//   },
//   body : SignupComponent
// });
// console.log (response.data);
// dispatch({
//   type : REGISTER_SUCCESS,
//   payload : response.data
// });
// } catch (error) {
// console.log (error.response.data);
// dispatch({
//   type : REGISTER_FAIL
// })
// }