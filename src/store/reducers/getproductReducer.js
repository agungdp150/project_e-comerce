import {GET_PRODUCT} from "../../types"

const initialState ={
  products : []
}

export default function(state = initialState, action) {
  const {type} = action;

  switch(type) {
    case GET_PRODUCT :
      return {
        ...state,
        ...action.payload,
        products : action.payload
      };
      default :
      return state;
  }
}