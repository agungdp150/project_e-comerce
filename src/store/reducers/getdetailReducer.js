import {GET_PRODUCT_DETAIL} from "../../types";

const initialState = {
  details : {}
}

export default function(state = initialState, action) {
  const { type, payload } = action

  switch(type) {
    case GET_PRODUCT_DETAIL:
      return {
        ...state,
        ...payload,
        details: payload.data
      }
    default:
      return state
  }
}

