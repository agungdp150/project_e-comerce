import { combineReducers } from "redux";
import regisReducer from "./regisReducer";
import loginReducer from "./loginReducer";
import getproductReducer from "./getproductReducer";
import getdetailReducer from "./getdetailReducer";

export default combineReducers ({
  register1 : regisReducer,
  login1 : loginReducer,
  product1 : getproductReducer,
  getdetail1 : getdetailReducer
});